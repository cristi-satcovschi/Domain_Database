'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

/**
 * Domain Schema
 */

const DomainSchema = new Schema({
  ns_dns: { type: String, default: '', trim: true },
  ns_ip: { type: Boolean, default: false },
  ns_searched: { type: String, default: '', trim: true },
  ns_ip: { type: String, default: '', trim: true },
  hosted_domain_name: { type: String, default: '', trim: true },
  hosted_domain_ip: { type: String, default: '', trim: true },
  hosted_domain_ip_country: { type: String, default: '', trim: true },
  hosted_domain_active: { type: Boolean, default: false },
  source_dnstree: { type: Boolean, default: false },
  source_viewdns: { type: Boolean, default: false },
  created: { type : Date, default : Date.now },
  last_verified: { type : Date, default : Date.now },
  current: { type: Boolean, default: false }
});

/**
 * Methods
 */

DomainSchema.methods = {
  addDomainByIp: function () {},
  addDomainByName: function () {},
  removeDomain: function () {},
  updateDomain: function () {}
};

/**
 * Statics
 */

DomainSchema.statics = {
  
    /**
     * Find domain by ip
     */
  
    findByIp: function (_ip) {
      return this.find({ name: new RegExp(_ip, 'i')}).exec();
    },
  
    /**
     * Find domain by name
     */
  
    findByName: function (_name) {
      return this.find({ name: new RegExp(_name, 'i')}).exec();
    },
  };


mongoose.model('Domain', DomainSchema);