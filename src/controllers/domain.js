'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const { wrap: async } = require('co');
const only = require('only');
const { respond, respondOrRedirect } = require('../utils');

/**
 * Search
 */

exports.find = async(function* (req, res) {
  const page = (req.query.page > 0 ? req.query.page : 1) - 1;
  const type = req.query.type;
  const limit = 30;
  const options = {
    limit: limit,
    page: page
  };

  if (type == 'id') {
      options.criteria = { _id };
      const domain = yield Domain.findById(options);
  } else {
      options.criteria = { _name };
      const domain = yield Domain.findByName(options);
  }

  const count = yield Domain.count();

  respond(res, 'domains/find', {
    title: 'Domains',
    domains: domains,
    page: page + 1,
    pages: Math.ceil(count / limit)
  });
});

/**
 * New domain
 */

exports.new = function (req, res){
  res.render('articles/new', {
    domain: new Domain()
  });
};

/**
 * Add a domain by IP
 */

exports.addByIp = async(function* (req, res) {
  const domain = new Domain(only(req.body, 'ip'));
  try {
    yield domain.addDomainByIp(req.file);
    respondOrRedirect({ req, res }, `/domains/${domain._id}`, domain, {
      type: 'success',
      text: 'Successfully added domain!'
    });
  } catch (err) {
    respond(res, 'domains/new', {
      ip: domain.id || 'New Domain',
      errors: [err.toString()],
      domain
    }, 422);
  }
});

/**
 * Add a domain by name
 */

exports.addByName = async(function* (req, res) {
  const domain = new Domain(only(req.body, 'name'));
  try {
    yield domain.addDomainByName(req.file);
    respondOrRedirect({ req, res }, `/domains/${domain._id}`, domain, {
      type: 'success',
      text: 'Successfully added domain!'
    });
  } catch (err) {
    respond(res, 'domains/new', {
      name: domain.name || 'New Domain',
      errors: [err.toString()],
      domain
    }, 422);
  }
});

/**
 * Update a domain
 */

exports.update = async(function* (req, res){
});

/**
 * Delete a domain
 */

exports.destroy = async(function* (req, res) {
  yield req.domain.remove();
  respondOrRedirect({ req, res }, '/domains', {}, {
    type: 'info',
    text: 'Deleted successfully'
  });
});