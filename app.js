'use strict';

/**
 * Module dependencies
 */

const fs = require('fs');
const join = require('path').join;
const restify = require('restify');
const mongoose = require('mongoose');
const assert = require('assert');

const models = join(__dirname, 'src/models');

// Bootstrap models
fs.readdirSync(models)
    .filter(file => ~file.search(/^[^\.].*\.js$/))
    .forEach(file => require(join(models, file)));

mongoose.connect('mongodb://mongo:27017/domaindb', { useMongoClient: true });
mongoose.Promise = global.Promise;

const app = restify.createServer({
    name: 'DomainDatabase',
    version: '1.0.0'
});

app.use(restify.plugins.acceptParser(app.acceptable));
app.use(restify.plugins.queryParser());
app.use(restify.plugins.bodyParser());

app.get('/domains/new', function (req, res, next) {
    res.send(401)
    return next();
});

app.listen(8080, function () {
    console.log('%s listening at %s', app.name, app.url);
});

module.exports = app;