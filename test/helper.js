'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const Domain = mongoose.model('Domain');
const co = require('co');

/**
 * Clear database
 */

exports.cleanup = function (t) {
  co(function* () {
    yield Domain.remove();
    t.end();
  });
};