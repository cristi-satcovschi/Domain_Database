'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const test = require('tape');
const request = require('supertest');
const app = require('../app');
const { cleanup } = require('./helper');
const Domain = mongoose.model('Domain');
const agent = request.agent(app);

const _domain = {
    name: 'google.com'
};

test('Clean up', cleanup);

test('Create domain', async t => {
    const domain = new Domain(_domain);
    return await domain.save(t.end);
});

test('POST /domains - when not logged in', t => {
    agent
        .get('/domains/new')
        .expect(401)
        .end(t.end);
});

test('Clean up', cleanup);

test.onFinish(() => process.exit(0));